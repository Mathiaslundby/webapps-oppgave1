﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webapps_Mappe1.Model;
using Webapps_Mappe1.DAL;

namespace Webapps_Mappe1.DAL
{
    public interface IDepartureDAL
    {

        List<Departure> GetAllDepartures();

        Departure GetDeparture(int id);

        bool CreateDeparture(DepartureModel departure);
        bool DeleteDeparture(int id);

        bool EditDeparture(int id, Departure departure);
        bool CreateDepartures();
        List<Departure> GetDepartures(DepartureModel departure);
    }
}
