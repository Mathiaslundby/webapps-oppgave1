﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webapps_Mappe1.Model;


namespace Webapps_Mappe1.DAL
{
    public class TicketDALStub : ITicketDAL
    {
        public bool createNewTicket(Ticket postTicket)
        {
            if (postTicket.NumStudents == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public DBTicket findTicket(int id)
        {
            if (id == 0)
            {
                return null;
            }
            else
            {
                return new DBTicket();
            }
        }

        public List<DBTicket> GetTickets()
        {
            var list = new List<DBTicket>();
            var ticket = new DBTicket()
            {
                NumStudents = 1,
            };

            list.Add(ticket);
            list.Add(ticket);
            list.Add(ticket);

            return list;
        }
    }
}
