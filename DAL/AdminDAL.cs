﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webapps_Mappe1.DAL;
using Webapps_Mappe1.Model;

namespace Webapps_Mappe1.DAL
{
    public class AdminDAL
    {
        public bool CreateAdmin() //Metode for å oprette Admin i databasen
        {
            var db = new DBContext();

            var newUser = new DBUser();
            byte[] passwordDb = MakeHash("admin");
            newUser.Password = passwordDb;
            newUser.Name = "admin";
            newUser.Role = "admin";
            db.Users.Add(newUser);
            try
            {
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
   

        private static byte[] MakeHash(string innPassord)
        {
            byte[] inData, outData;
            var alg = System.Security.Cryptography.SHA256.Create();
            inData = Encoding.ASCII.GetBytes(innPassord);
            outData = alg.ComputeHash(inData);
            return outData;
        }

        public DBUser UserInDB(Admin inAdmin)
        {
            using (var db = new DBContext())
            {
                byte[] passwordDb = MakeHash(inAdmin.Password);
                DBUser foundUser = db.Users.FirstOrDefault
                (b => b.Password == passwordDb && b.Name == inAdmin.Name);
                if (foundUser == null)
                {
                    return null; 
                }
                else
                {
                    return foundUser;
                }
            }
        }
        public bool CheckAdminExists()
        {
            using (var db = new DBContext())
            {
                if(db.Users.Count() == 0){
                    return true;
                }
                return false;
            }
        }


        public void Reset()
        {
            var db = new DBContext();
            db.Database.Delete();
        }
    }
}
