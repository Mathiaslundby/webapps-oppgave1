﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Webapps_Mappe1.Model;

namespace Webapps_Mappe1.DAL
{
    public class DBContext : DbContext
    {
        public DBContext() : base("name=Ticket")
        {
            //Database.Delete();
            Database.CreateIfNotExists();
        }

        public void reset()
        {
            Database.Delete();
        }

        public DbSet<DBTicket> Tickets { get; set; }
        public DbSet<Station> Stations { get; set; }
        public DbSet<Departure> Departures { get; set; }
        public DbSet<DBUser> Users { get; set; }
        public DbSet<Price> Prices { get; set; }

        public override int SaveChanges()
        {
            var addedAuditedEntities = ChangeTracker.Entries<IAuditedEntity>()
              .Where(p => p.State == EntityState.Added)
              .Select(p => p.Entity);

            var modifiedAuditedEntities = ChangeTracker.Entries<IAuditedEntity>()
              .Where(p => p.State == EntityState.Modified)
              .Select(p => p.Entity);

            var now = DateTime.UtcNow;

            foreach (var added in addedAuditedEntities)
            {
                added.CreatedAt = now;
                added.LastModifiedAt = now;
            }

            foreach (var modified in modifiedAuditedEntities)
            {
                modified.LastModifiedAt = now;
            }
            return base.SaveChanges();
        }
        

    }
}