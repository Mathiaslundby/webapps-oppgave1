﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webapps_Mappe1.Model;
namespace Webapps_Mappe1.DAL
{
    public class DepartureDAL : IDepartureDAL
    {
        DBContext db = new DBContext();
        
        public List<Departure> GetAllDepartures() => db.Departures.Where(d => d.Active).ToList();

        public Departure GetDeparture(int id) => db.Departures.Find(id);

        public List<Departure> GetDepartures(DepartureModel departure) => db.Departures.Where(
                d => d.StartStation.Name == departure.StartStation &&
                     d.EndStation.Name == departure.EndStation &&
                     d.Active
            ).ToList();

        public bool CreateDeparture(DepartureModel departure)
        {

            Station startStation = db.Stations.FirstOrDefault(s => s.Name == departure.StartStation);
            Station endStation = db.Stations.FirstOrDefault(s => s.Name == departure.EndStation);

            Departure insertDeparture = new Departure();
            insertDeparture.StartTime = departure.StartTime;
            insertDeparture.StartStation = startStation;
            insertDeparture.EndStation = endStation;
            insertDeparture.Active = true;
            insertDeparture.CreatedBy = departure.CreatedBy;
            db.Departures.Add(insertDeparture);
            try
            {
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
            }

        public bool DeleteDeparture(int id)
        {
            Departure departure = db.Departures.Find(id);
            departure.Active = false;
            try
            {
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public bool EditDeparture(int id, Departure departure)
        {
            Departure editDeparture = db.Departures.Find(id);
            editDeparture.StartTime = departure.StartTime;
            editDeparture.LastModifiedBy = departure.LastModifiedBy;
            try
            {
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public void Reset()
        {
            db.reset();
        }

        public bool CreateDepartures()
        {
            DBContext db = new DBContext();
            List<Station> sList = db.Stations.ToList();
            Departure d;

            for (int i = 0; i < sList.Count(); i++)
            {
                for (int j = 0; j < sList.Count(); j++)
                {
                    if (i == j) continue;

                    Random r = new Random();
                    string num = "" + r.Next(0, 59);
                    if (num.Length == 1) num = "0" + num;

                    for (int k = 0; k < 24; k++)
                    {
                        d = new Departure();
                        d.StartStation = sList[i];
                        d.EndStation = sList[j];

                        string time = "";
                        if (k < 10) time += "0";
                        time += k + ":" + num;

                        d.StartTime = time;
                        d.Active = true;
                        db.Departures.Add(d);
                    }
                }
            }
            try
            {
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}
