﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webapps_Mappe1.Model;

namespace Webapps_Mappe1.DAL
{
    public class TicketDAL : ITicketDAL
    {
        DBContext db = new DBContext();
        public List<DBTicket> GetTickets() => db.Tickets.ToList();

        public DBTicket findTicket(int id) => db.Tickets.Find(id);

        public bool createNewTicket(Ticket postTicket)
        {
            string now = DateTime.Now.ToString("dd.MM.yyyy HH:mm");
            DBTicket ticket = new DBTicket();

            ticket.StartDate = postTicket.StartDate;
            ticket.RoundTripDate = postTicket.RoundTripDate;
            ticket.Email = postTicket.Email;
            ticket.TimeBought = now;
            ticket.NumAdults = postTicket.NumAdults;
            ticket.NumChildren = postTicket.NumChildren;
            ticket.NumStudents = postTicket.NumStudents;
            ticket.NumSeniors = postTicket.NumSeniors;

            int totalPrice = 0;
            totalPrice += (ticket.NumAdults * db.Prices.Find(1).Value);
            totalPrice += (ticket.NumChildren * db.Prices.Find(2).Value);
            totalPrice += (ticket.NumStudents * db.Prices.Find(3).Value);
            totalPrice += (ticket.NumSeniors * db.Prices.Find(4).Value);

            Departure departure = db.Departures.FirstOrDefault(d => d.StartStation.Name == postTicket.StartStation &&
                    d.EndStation.Name == postTicket.EndStation &&
                    d.StartTime == postTicket.StartTime);
            ticket.Departure = departure;

            if (postTicket.RoundTrip)
            {
                Departure roundTrip = db.Departures.FirstOrDefault(d => d.EndStation.Name == postTicket.StartStation &&
                    d.StartStation.Name == postTicket.EndStation &&
                    d.StartTime == postTicket.RoundTripTime);
                ticket.RoundTrip = roundTrip;
                totalPrice *= 2;
            }
            ticket.Price = totalPrice;

            db.Tickets.Add(ticket);
            try
            {
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}
