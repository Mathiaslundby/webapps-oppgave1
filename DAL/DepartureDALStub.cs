﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webapps_Mappe1.Model;


namespace Webapps_Mappe1.DAL
{
    public class DepartureDALStub : IDepartureDAL
    {
        public bool CreateDeparture(DepartureModel departure)
        {
            if (departure.StartStation == "startstation")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool CreateDepartures()
        {
            throw new NotImplementedException();
        }

        public bool DeleteDeparture(int id)
        {
            if (id == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool EditDeparture(int id, Departure departure)
        {
            if (id == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public List<Departure> GetAllDepartures()
        {
            Departure dep = new Departure()
            {
                Active = true,
            };

            var liste = new List<Departure>();
            liste.Add(dep);
            liste.Add(dep);
            liste.Add(dep);

            return liste;
        }

        public Departure GetDeparture(int id)
        {
            Departure dep = new Departure()
            {
                Active = true,
            };

            if (id == 0)
            {
                return null;
            }
            else
            {
                return dep;
            }
        }

        public List<Departure> GetDepartures(DepartureModel departure)
        {
            return null;
        }
    }
}
