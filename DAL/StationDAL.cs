﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webapps_Mappe1.Model;

namespace Webapps_Mappe1.DAL
{
    public class StationDAL : IStationDAL
    {
        private DBContext db;
        public StationDAL()
        {
            db = new DBContext();
        }
        public List<Station> GetAllStations() => db.Stations.Where(s => s.Active).ToList();

        public Station GetStation(int id) => db.Stations.Find(id);

        public bool CreateStation(Station station)
        {
            List<Station> stations = GetAllStations();
            station.Active = true;
            station.CreatedBy = station.CreatedBy;
            db.Stations.Add(station);

            foreach (Station s in stations)
            {
                Random r = new Random();
                string num = "" + r.Next(0, 59);
                if (num.Length == 1) num = "0" + num;

                for (int i = 0; i < 24; i++)
                {
                    String time = "";
                    if (i < 10)
                    {
                        time += "0";
                    }
                    time += i + ":" + num;

                    Departure departure = new Departure
                    {
                        StartStation = s,
                        EndStation = station,
                        StartTime = time,
                        Active = true
                    };
                    db.Departures.Add(departure);
                }

                for (int i = 0; i < 24; i++)
                {
                    String time = "";
                    if (i < 10)
                    {
                        time += "0";
                    }
                    time += i + ":" + num;

                    Departure roundTrip = new Departure
                    {
                        StartStation = station,
                        EndStation = s,
                        StartTime = time,
                        Active = true
                    };
                    db.Departures.Add(roundTrip);
                }
            }
            try
            {
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public bool DeleteStation(int id)
        {
            DepartureDAL departureDAL = new DepartureDAL();
            Station station = GetStation(id);
            
            List<Departure> departures = departureDAL.GetAllDepartures();
            
            station.Active = false;
            
            foreach (Departure d in departures)
            {
                if(d.StartStation.ID == station.ID || d.EndStation.ID == station.ID)
                {
                    departureDAL.DeleteDeparture(d.ID);
                }
            }

            try
            {
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public bool EditStation(int id, Station station)
        {
            Station editStation = GetStation(id);
            editStation.Name = station.Name;
            editStation.LastModifiedBy = station.LastModifiedBy;
            try
            {
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        
        public void CreateStations()
        {
            string line;

            String path = "";  //Endres til din path når du tester, settes blank når du pusher
            /*
             * LA STÅ 
             * Alle adder sin absolut path som kommentar
             * @"C:\Users\mathi\source\repos\webapps-oppgave1\Webapps-Mappe1\stations.txt"
             * 
             */

            System.IO.StreamReader file =
                new System.IO.StreamReader(path);
            while ((line = file.ReadLine()) != null)
            {
                Station s = new Station();
                s.Name = line;
                s.Active = true;
                db.Stations.Add(s);
                db.SaveChanges();

            }
        }
       
        public bool CheckStationsNotExists()
        {
            try
            {
                return db.Stations.Count() == 0;
            }
            catch (Exception)
            {
                return true;
            }
        }
    }
}
