﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webapps_Mappe1.Model;
using Webapps_Mappe1.DAL;
namespace Webapps_Mappe1.DAL
{
    public interface IStationDAL
    {
        bool CreateStation(Station station);
        bool DeleteStation(int id);
        bool EditStation(int id, Station station);
        List<Station> GetAllStations();
        Station GetStation(int id);
    }
}