using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webapps_Mappe1.Model;

namespace Webapps_Mappe1.DAL
{
    public class PriceDAL : IPriceDAL
    {
        DBContext db = new DBContext();


        public List<Price> GetAllPrices() => db.Prices.ToList();
        public Price GetPrice(int id) => db.Prices.Find(id);

        public bool EditPrice(int id, Price inPrice)
        {
            Price editPrice = GetPrice(id);
            editPrice.Value = inPrice.Value;
            editPrice.LastModifiedBy = inPrice.LastModifiedBy;
            try
            {
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public bool CreatePrices()
        {
            var db = new DBContext();
            Price price = new Price
            {
                Type = "Adult",
                Value = 120
            };
            db.Prices.Add(price);

            price = new Price
            {
                Type = "Child",
                Value = 40
            };
            db.Prices.Add(price);

            price = new Price
            {
                Type = "Student",
                Value = 85
            };
            db.Prices.Add(price);

            price = new Price
            {
                Type = "Senior",
                Value = 40
            };
            db.Prices.Add(price);

            try
            {
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}
