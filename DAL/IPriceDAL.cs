﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webapps_Mappe1.Model;
using Webapps_Mappe1.DAL;

namespace Webapps_Mappe1.DAL
{
    public interface IPriceDAL
    {
        List<Price> GetAllPrices();
        Price GetPrice(int id);
        bool EditPrice(int id, Price inPrice);
    }
}
