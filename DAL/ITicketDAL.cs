﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webapps_Mappe1.Model;
using Webapps_Mappe1.DAL;

namespace Webapps_Mappe1.DAL
{
    public interface ITicketDAL
    {
        List<DBTicket> GetTickets();

        DBTicket findTicket(int id);

        bool createNewTicket(Ticket postTicket);
    }
}
