﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webapps_Mappe1.Model;


namespace Webapps_Mappe1.DAL
{
    public class PriceDALStub : IPriceDAL
    {
        public List<Price> GetAllPrices()
        {
            var List = new List<Price>();
            var price = new Price()
            {
                Type = "Student",
                Value = 110,
            };
            List.Add(price);
            List.Add(price);
            List.Add(price);

            return List;
        }
        public Price GetPrice(int id)
        {
            if (id == 0)
            {
                return null;
            }
            else
            {
                return new Price();
            }
        }

        public bool EditPrice(int id, Price inPrice)
        {
            if(id == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
