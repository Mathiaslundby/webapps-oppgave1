﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webapps_Mappe1.Model;


namespace Webapps_Mappe1.DAL
{
    public class StationDALStub : IStationDAL
    {
        public List<Station> GetAllStations()
        {
            var stationList = new List<Station>();
            var station = new Station()
            {
                Name = "TestStation",
                Active = true,
            };
            stationList.Add(station);
            stationList.Add(station);
            stationList.Add(station);

            return stationList;
        }

        public Station GetStation(int id)
        {
            var station = new Station()
            {
                ID = 1,
                Name = "TestStation",
                Active = true,
            };

            if (id == 0)
            {
                return null;
            }
            else
            {
                return station;
            }
        }

        public bool CreateStation(Station station)
        {
            if (station.Active == true)
            {
                return true;
            }
            else
            {
                return false;
            }          
        }

        public bool DeleteStation(int id)
        {
            if (id == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool EditStation(int id, Station station)
        {
            if (id == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
