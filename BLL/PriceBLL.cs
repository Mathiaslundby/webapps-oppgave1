﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webapps_Mappe1.DAL;
using Webapps_Mappe1.Model;

namespace Webapps_Mappe1.BLL
{
    public class PriceBLL
    {
        private IPriceDAL _priceDAL;

        public PriceBLL()
        {
            _priceDAL = new PriceDAL();
        }

        public PriceBLL(PriceDALStub stub)
        {
            _priceDAL = stub;
        }
        public List<Price> GetAllPrices()
        {
            
            return _priceDAL.GetAllPrices();
        }
        public Price GetPrice(int id)
        {
            return _priceDAL.GetPrice(id);
        }

        public bool EditPrice(int id, Price inPrice)
        {
            return (_priceDAL.EditPrice(id, inPrice));
        }
        
        public bool CreatePrices()
        {
            PriceDAL priceDAL = new PriceDAL();
            return (priceDAL.CreatePrices());
        }
    }
}
