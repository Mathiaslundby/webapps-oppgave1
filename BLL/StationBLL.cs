﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webapps_Mappe1.DAL;
using Webapps_Mappe1.Model;

namespace Webapps_Mappe1.BLL
{
    public class StationBLL
    {
        private IStationDAL _stationDAL;
        public StationBLL()
        {
            _stationDAL = new StationDAL();
        }

        public StationBLL(StationDALStub stationDALStub)
        {
            _stationDAL = stationDALStub;
        }

        public List<Station> GetAllStations()
        {
            return _stationDAL.GetAllStations();
        }

        public Station GetStation(int id)
        {
            return _stationDAL.GetStation(id);
        }

        public bool CreateStation(Station station)
        {
            return _stationDAL.CreateStation(station);
        }

        public bool DeleteStation(int id)
        {
            return _stationDAL.DeleteStation(id);
        }

        public bool EditStation(int id, Station station)
        {
            return _stationDAL.EditStation(id, station);
        }
/*
        public void CreateStations()
        {
            _stationDAL.CreateStations();
        }
        public bool CheckStationsNotExists()
        {
            return _stationDAL.CheckStationsNotExists();
        }*/
    }
}
