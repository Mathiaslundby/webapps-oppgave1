﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webapps_Mappe1.DAL;
using Webapps_Mappe1.Model;

namespace Webapps_Mappe1.BLL
{
    public class DepartureBLL
    {
        IDepartureDAL _departureDAL;
        public DepartureBLL()
        {
            _departureDAL = new DepartureDAL();
        }

        public DepartureBLL(DepartureDALStub stub)
        {
            _departureDAL = stub;
        }
        public List<Departure> GetAllDepartures()
        {
            return _departureDAL.GetAllDepartures();
        }

        public Departure GetDeparture(int id)
        {
            return _departureDAL.GetDeparture(id);
        }
        public List<Departure> GetDepartures(DepartureModel departure)
        {  
            return _departureDAL.GetDepartures(departure);
        }

        public bool CreateDeparture(DepartureModel departure)
        {
           return _departureDAL.CreateDeparture(departure);
        }

        public bool DeleteDeparture(int id)
        {
           return _departureDAL.DeleteDeparture(id);
        }

        public bool EditDeparture(int id, Departure departure)
        {
           return _departureDAL.EditDeparture(id, departure);
        }

        public void Reset() //FOR TESTING PURPOSES
        {
            DepartureDAL departureDALdb = new DepartureDAL();
            departureDALdb.Reset();
        }
        public bool CreateDepartures()
        {
           return _departureDAL.CreateDepartures();
        }

        
    }
}
