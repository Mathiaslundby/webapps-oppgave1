﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webapps_Mappe1.DAL;
using Webapps_Mappe1.Model;

namespace Webapps_Mappe1.BLL
{
    public class AdminBLL
    {
        AdminDAL adminDAL = new AdminDAL();
        public void createAdmin()
        {
            adminDAL.CreateAdmin();
        }

        public DBUser UserInDB(Admin user)
        {
            return adminDAL.UserInDB(user);
        }
        public bool CheckAdminExists()
        {
            return adminDAL.CheckAdminExists();
        }

        public void Reset()
        {
            adminDAL.Reset();
        }
    }
}
