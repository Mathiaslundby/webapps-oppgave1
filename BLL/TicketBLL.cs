﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webapps_Mappe1.DAL;
using Webapps_Mappe1.Model;

namespace Webapps_Mappe1.BLL
{
    public class TicketBLL
    {
        private ITicketDAL _ticketDAL;

        public TicketBLL()
        {
            _ticketDAL = new TicketDAL();
        }

        public TicketBLL(TicketDALStub stub)
        {
            _ticketDAL = stub;
        }
        public List<DBTicket> GetTickets()
        {
            return _ticketDAL.GetTickets();
        }

        public DBTicket findTicket(int id)
        {
            return _ticketDAL.findTicket(id);
        }

        public bool createNewTicket(Ticket postTicket)
        {
            return _ticketDAL.createNewTicket(postTicket);
        }
    }
}
