﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Webapps_Mappe1.BLL;
using Webapps_Mappe1.Controllers;
using Webapps_Mappe1.DAL;
using Webapps_Mappe1.Model;

namespace UnitTest
{
    [TestClass]
    public class UnitTestPrice
    {
        PriceController priceController = new PriceController(new PriceBLL(new PriceDALStub()));

        [TestMethod]
        public void Get_list_view()
        {
            //Arrange
            var expectedResults = new List<Price>();
            var price = new Price()
            {
                Type = "Student",
                Value = 110,
            };
            expectedResults.Add(price);
            expectedResults.Add(price);
            expectedResults.Add(price);

            //Act
            var result = (ViewResult)priceController.Index();
            var resultList = (List<Price>)result.Model;
            //Assert
            Assert.AreEqual(result.ViewName, "");

            for (var i = 0; i < resultList.Count; i++)
            {
                Assert.AreEqual(expectedResults[i].Type, resultList[i].Type);
                Assert.AreEqual(expectedResults[i].Value, resultList[i].Value);
            }
        }

        [TestMethod]
        public void Editprice_ok_post()
        {
            var price = new Price()
            {
                ID = 1,
                Type = "Student",
                Value = 110,
            };

            var result = (RedirectToRouteResult)priceController.EditPrice(price.ID, price);

            Assert.AreEqual(result.RouteName, "");
            
        }

        [TestMethod]
        public void Editprice_show_view()
        {

            //Act
            var result = (ViewResult)priceController.EditPrice(1);
            //Assert
            Assert.AreEqual(result.ViewName, "");
        }

        [TestMethod]
        public void Editprice_error_DB_post()
        {
            var price = new Price();

 
            var result = (ViewResult)priceController.EditPrice(0, price);

            Assert.AreEqual(result.ViewName, "");

        }

        [TestMethod]
        public void Editprice_error_validating_post()
        {
            var price = new Price();
            priceController.ViewData.ModelState.AddModelError("type", "ikke oppgitt noe pris type");


            var result = (ViewResult)priceController.EditPrice(0, price);

            Assert.IsTrue(result.ViewData.ModelState.Count == 1);
            Assert.AreEqual(result.ViewName, "");

        }

    }
}
