﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Webapps_Mappe1.BLL;
using Webapps_Mappe1.Controllers;
using Webapps_Mappe1.DAL;
using Webapps_Mappe1.Model;

namespace UnitTest
{
    [TestClass]
    public class UnitTestTicket
    {

        TicketController ticketController = new TicketController(new TicketBLL(new TicketDALStub()));

        [TestMethod]
        public void Test_index_show_view()
        {
            var result = (ViewResult) ticketController.Index();

            Assert.AreEqual(result.ViewName, "");
        }
        [TestMethod]
        public void Get_list_view()
        {
            //Arrange
            var expectedResults = new List<DBTicket>();
            var ticket = new DBTicket()
            {
                NumStudents = 1,
            };
            expectedResults.Add(ticket);
            expectedResults.Add(ticket);
            expectedResults.Add(ticket);

            //Act
            var result = (ViewResult)ticketController.ShowTickets();
            var resultList = (List<DBTicket>)result.Model;
            //Assert
            Assert.AreEqual(result.ViewName, "");

            for (var i = 0; i < resultList.Count; i++)
            {
                Assert.AreEqual(expectedResults[i].NumStudents, resultList[i].NumStudents);
            }
        }

        [TestMethod]
        public void index_riktig_view()
        {
            var ticket = new Ticket()
            {
                NumStudents = 1,
            };

            var result = (RedirectToRouteResult)ticketController.Index(ticket);

            Assert.AreEqual(result.RouteName, "");
        }

        [TestMethod]
        public void Index_error_DB_post()
        {
            var ticket = new Ticket()
            {
                NumStudents = 0,
            };


            var result = (ViewResult)ticketController.Index(ticket);

            Assert.AreEqual(result.ViewName, "");

        }

        [TestMethod]
        public void Editprice_error_validating_post()
        {
            var ticket = new Ticket();
            ticketController.ViewData.ModelState.AddModelError("type", "ikke oppgitt noe pris type");


            var result = (ViewResult)ticketController.Index(ticket);

            Assert.IsTrue(result.ViewData.ModelState.Count == 1);
            Assert.AreEqual(result.ViewName, "");

        }

        public void find_ticket_ok()
        {
            
            var result = (ViewResult)ticketController.Details(1);


            Assert.AreEqual(result.ViewName, "");
        }
    }
}
