﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Webapps_Mappe1.BLL;
using Webapps_Mappe1.Controllers;
using Webapps_Mappe1.DAL;
using Webapps_Mappe1.Model;

namespace UnitTest
{
    [TestClass]
    public class UnitTestStation
    {
        StationController stationController = new StationController(new StationBLL(new StationDALStub()));

        [TestMethod]
        public void get_list_view()
        {
            //Arrange
            var expectedResults = new List<Station>();
            var station = new Station()
            {
                Name = "TestStation",
                Active = true,
            };
            expectedResults.Add(station);
            expectedResults.Add(station);
            expectedResults.Add(station);

            //Act
            var result = (ViewResult)stationController.Index();
            var resultList = (List<Station>)result.Model;
            //Assert
            Assert.AreEqual(result.ViewName, "");

            for (var i = 0; i < resultList.Count; i++)
            {
                Assert.AreEqual(expectedResults[i].Name, resultList[i].Name);
                Assert.AreEqual(expectedResults[i].Active, resultList[i].Active);
            }
        }


        [TestMethod]
        public void Editstation_show_view()
        {
            //act
            var result = (ViewResult)stationController.EditStation(1);
            //assert
            Assert.AreEqual(result.ViewName, "");
        }

        [TestMethod]
        public void Create_station_show_view()
        {
            //act
            var result = (ViewResult)stationController.CreateStation();
            //assert
            Assert.AreEqual(result.ViewName, "");
        }

        [TestMethod]
        public void Create_station_ok()
        {
            var station = new Station()
            {
                ID = 1,
                Name = "TestStation",
                Active = true,
            };

            //act
            var result = (RedirectToRouteResult)stationController.CreateStation(station);
            //assert
            Assert.AreEqual(result.RouteName, "");
        }

        [TestMethod]
        public void Create_error_DB_post()
        {
            var station = new Station();


            var result = (ViewResult)stationController.CreateStation(station);

            Assert.AreEqual(result.ViewName, "");

        }

        [TestMethod]
        public void Createstation_error_validating_post()
        {
            var station = new Station();
            stationController.ViewData.ModelState.AddModelError("active", "ikke oppgitt om den er aktiv");


            var result = (ViewResult)stationController.CreateStation(station);

            Assert.IsTrue(result.ViewData.ModelState.Count == 1);
            Assert.AreEqual(result.ViewName, "");

        }


        [TestMethod]
        public void Edit_station_ok()
        {
            var station = new Station()
            {
                ID = 1,
                Name = "TestStation",
                Active = true,
            };
            //act
            var result = (RedirectToRouteResult)stationController.EditStation(1, station);
            //assert
            Assert.AreEqual(result.RouteName, "");
        }

        [TestMethod]
        public void Edit_station_error_DB_post()
        {
            var station = new Station();


            var result = (ViewResult)stationController.EditStation(0, station);

            Assert.AreEqual(result.ViewName, "");

        }

        [TestMethod]
        public void Edit_station_error_validating_post()
        {
            var station = new Station();
            stationController.ViewData.ModelState.AddModelError("Active", "ikke oppgitt om den er aktiv");


            var result = (ViewResult)stationController.EditStation(1, station);

            Assert.IsTrue(result.ViewData.ModelState.Count == 1);
            Assert.AreEqual(result.ViewName, "");

        }

    }
}
