﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Web.Mvc;
using Webapps_Mappe1.BLL;
using Webapps_Mappe1.Controllers;
using Webapps_Mappe1.DAL;
using Webapps_Mappe1.Model;

namespace UnitTest
{
    [TestClass]
    public class UnitTestDeparture
    {

        DepartureController departureController = new DepartureController(new DepartureBLL(new DepartureDALStub()));

        [TestMethod]
        public void Edit_departure_show_view()
        {
            var result = (ViewResult) departureController.EditDeparture(1);

            Assert.AreEqual(result.ViewName, "");
        }

        [TestMethod]
        public void Create_departure_show_view()
        {
            //act
            var result = (ViewResult)departureController.CreateDeparture();
            //assert
            Assert.AreEqual(result.ViewName, "");
        }

        [TestMethod]
        public void get_list_view()
        {
            //Arrange
            var expectedResults = new List<Departure>();
            var departure = new Departure()
            {
                Active = true,
            };
            expectedResults.Add(departure);
            expectedResults.Add(departure);
            expectedResults.Add(departure);

            //Act
            var result = (ViewResult)departureController.Index();
            var resultList = (List<Departure>)result.Model;
            //Assert
            Assert.AreEqual(result.ViewName, "");

            for (var i = 0; i < resultList.Count; i++)
            {
                Assert.AreEqual(expectedResults[i].Active, resultList[i].Active);
            }
        }

        [TestMethod]
        public void Create_departure_ok()
        {
            var dep = new DepartureModel()
            {
                StartStation = "startstation",
            };

            //act
            var result = (RedirectToRouteResult)departureController.CreateDeparture(dep);
            //assert
            Assert.AreEqual(result.RouteName, "");
        }

        [TestMethod]
        public void Create_error_DB_post()
        {
            var dep = new DepartureModel();


            var result = (ViewResult)departureController.CreateDeparture(dep);

            Assert.AreEqual(result.ViewName, "");

        }

        [TestMethod]
        public void CreateDeparture_error_validating_post()
        {
            var departure = new DepartureModel();
            departureController.ViewData.ModelState.AddModelError("active", "ikke oppgitt om den er aktiv");


            var result = (ViewResult)departureController.CreateDeparture(departure);

            Assert.IsTrue(result.ViewData.ModelState.Count == 1);
            Assert.AreEqual(result.ViewName, "");
        }

        [TestMethod]
        public void Edit_departure_ok()
        {
            var dep = new Departure()
            {
                ID = 1,
            };
            //act
            var result = (RedirectToRouteResult)departureController.EditDeparture(dep.ID, dep);
            //assert
            Assert.AreEqual(result.RouteName, "");
        }

        [TestMethod]
        public void Edit_departure_error_DB_post()
        {
            var dep = new Departure();


            var result = (ViewResult)departureController.EditDeparture(0, dep);

            Assert.AreEqual(result.ViewName, "");

        }

        [TestMethod]
        public void Edit_departure_error_validating_post()
        {
            var dep = new Departure();
            departureController.ViewData.ModelState.AddModelError("Active", "ikke oppgitt om den er aktiv");


            var result = (ViewResult)departureController.EditDeparture(1, dep);

            Assert.IsTrue(result.ViewData.ModelState.Count == 1);
            Assert.AreEqual(result.ViewName, "");

        }
    }
}
