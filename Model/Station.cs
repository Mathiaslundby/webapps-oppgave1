﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Webapps_Mappe1.Model
{
    public class Station : IAuditedEntity
    {
        public int ID { get; set; }

        [Display(Name = "Stasjon")]
        [Required(ErrorMessage = "Feltet må fylles ut")]
        public string Name { get; set; }
        public bool Active { get; set; }

        [Display(Name = "Laget av")]
        public string CreatedBy { get; set; }
        [Display(Name = "Tid laget")]
        public DateTime CreatedAt { get; set; }
        [Display(Name = "Sist endret av")]
        public string LastModifiedBy { get; set; }
        [Display(Name = "Sist endret")]
        public DateTime LastModifiedAt { get; set; }
    }
}
