﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Webapps_Mappe1.Model
{
    public class Ticket : IAuditedEntity
    {
        public int ID { get; set; }

        [Display(Name = "E-post:")]
        [Required(ErrorMessage = "Feltet må fylles ut")]
        [RegularExpression(@"^[_A-Za-z0-9-\+]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})$")]
        public string Email { get; set; }

        [Display(Name = "Fra:")]
        [Required(ErrorMessage = "Feltet må fylles ut")]
        public string StartStation { get; set; }

        [Display(Name = "Til:")]
        [Required(ErrorMessage = "Feltet må fylles ut")]
        public string EndStation { get; set; }

        [Display(Name = "Dato:")]
        [Required(ErrorMessage = "Feltet må fylles ut")]
        [RegularExpression(@"^\d{2}.\d{2}\.\d{4}$")]
        public string StartDate { get; set; }

        [Display(Name = "Klokkeslett:")]
        [Required(ErrorMessage = "Feltet må fylles ut")]
        [RegularExpression(@"^\d{2}:\d{2}$")]
        public string StartTime { get; set; }

        [Display(Name = "Tur/Retur:")]
        public bool RoundTrip { get; set; }

        [Display(Name = "Retur dato")]
        [RegularExpression(@"^\d{2}.\d{2}\.\d{4}$")]
        public string RoundTripDate { get; set; }

        [Display(Name = "Retur tid:")]
        [RegularExpression(@"^\d{2}:\d{2}$")]
        public string RoundTripTime { get; set; }

        [Display(Name = "Kjøpt:")]
        public string TimeBought { get; set; }

        [Display(Name = "Voksne:")]
        [RegularExpression("^[0-9]$")]
        public int NumAdults { get; set; }

        [Display(Name = "Barn:")]
        [RegularExpression("^[0-9]$")]
        public int NumChildren { get; set; }

        [Display(Name = "Studenter:")]
        [RegularExpression("^[0-9]$")]
        public int NumStudents { get; set; }

        [Display(Name = "Honnør:")]
        [RegularExpression("^[0-9]$")]
        public int NumSeniors { get; set; }

        [Display(Name = "Laget av")]
        public string CreatedBy { get; set; }
        [Display(Name = "Tid laget")]
        public DateTime CreatedAt { get; set; }
        [Display(Name = "Sist endret av")]
        public string LastModifiedBy { get; set; }
        [Display(Name = "Sist endret")]
        public DateTime LastModifiedAt { get; set; }
    }
}