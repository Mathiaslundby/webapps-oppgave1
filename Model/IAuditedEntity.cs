﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Webapps_Mappe1.Model
{
    public interface IAuditedEntity
    {
        [Display(Name = "Laget av")]
        string CreatedBy { get; set; }
        [Display(Name = "Tid laget")]
        DateTime CreatedAt { get; set; }
        [Display(Name = "Sist endret av")]
        string LastModifiedBy { get; set; }
        [Display(Name = "Sist endret")]
        DateTime LastModifiedAt { get; set; }
    }
}
