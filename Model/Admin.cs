﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Webapps_Mappe1.Model
{
    public class Admin
    {
        [Required(ErrorMessage = "Navn må oppgis")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Passord må oppgis")]
        public string Password { get; set; }
    }

    public class DBUser : IAuditedEntity
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public byte[] Password { get; set; }
        public string Role { get; set; }

        [Display(Name = "Laget av")]
        public string CreatedBy { get; set; }
        [Display(Name = "Tid laget")]
        public DateTime CreatedAt { get; set; }
        [Display(Name = "Sist endret av")]
        public string LastModifiedBy { get; set; }
        [Display(Name = "Sist endret")]
        public DateTime LastModifiedAt { get; set; }

    }
}