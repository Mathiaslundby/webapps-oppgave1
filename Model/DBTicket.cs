﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Webapps_Mappe1.Model
{
    public class DBTicket : IAuditedEntity
    {
        public int ID { get; set; }

        public string StartDate { get; set; }

        public string RoundTripDate { get; set; }

        [Display(Name = "Email:")]
        public string Email { get; set; }

        [Display(Name = "Kjøpt:")]
        public string TimeBought { get; set; }

        [Display(Name = "Voksne:")]
        public int NumAdults { get; set; }

        [Display(Name = "Barn:")]
        public int NumChildren { get; set; }

        [Display(Name = "Studenter:")]
        public int NumStudents { get; set; }

        [Display(Name = "Honnører:")]
        public int NumSeniors { get; set; }

        [Display(Name = "Pris:")]
        public int Price { get; set; }

        public virtual Departure Departure { get; set; }

        public virtual Departure RoundTrip { get; set; }

        [Display(Name = "Laget av")]
        public string CreatedBy { get; set; }
        [Display(Name = "Tid laget")]
        public DateTime CreatedAt { get; set; }
        [Display(Name = "Sist endret av")]
        public string LastModifiedBy { get; set; }
        [Display(Name = "Sist endret")]
        public DateTime LastModifiedAt { get; set; }
    }
}