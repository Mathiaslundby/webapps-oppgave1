﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Webapps_Mappe1.Model
{
    public class DepartureModel : IAuditedEntity
    {
        public int ID { get; set; }

        [Display(Name = "Fra")]
        [Required(ErrorMessage = "Feltet må fylles ut")]
        public string StartStation { get; set; }

        [Display(Name = "Til")]
        [Required(ErrorMessage = "Feltet må fylles ut")]
        public string EndStation { get; set; }

        [Display(Name = "Tid")]
        [Required(ErrorMessage = "Feltet må fylles ut")]
        [RegularExpression(@"^[0-9]{2}:[0-9]{2}$")]
        public string StartTime { get; set; }

        [Display(Name = "Laget av")]
        public string CreatedBy { get; set; }
        [Display(Name = "Tid laget")]
        public DateTime CreatedAt { get; set; }
        [Display(Name = "Sist endret av")]
        public string LastModifiedBy { get; set; }
        [Display(Name = "Sist endret")]
        public DateTime LastModifiedAt { get; set; }
    }
}