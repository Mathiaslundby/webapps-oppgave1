﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Webapps_Mappe1.Model
{
    public class Departure : IAuditedEntity
    {
        public int ID { get; set; }

        [Display(Name = "Tid")]
        [RegularExpression(@"^[0-9]{2}:[0-9]{2}$", ErrorMessage = "Kontroller at tid er skrevet inn riktig")]
        public string StartTime { get; set; }

        [Display(Name = "Fra")]
        public virtual Station StartStation { get; set; }

        [Display(Name = "Til")]
        public virtual Station EndStation { get; set; }

        public bool Active { get; set; }

        [Display(Name = "Laget av")]
        public string CreatedBy { get; set; }
        [Display(Name = "Tid laget")]
        public DateTime CreatedAt { get; set; }
        [Display(Name = "Sist endret av")]
        public string LastModifiedBy { get; set; }
        [Display(Name = "Sist endret")]
        public DateTime LastModifiedAt { get; set; }
    }
}