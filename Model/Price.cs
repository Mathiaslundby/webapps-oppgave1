﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Webapps_Mappe1.Model
{
    public class Price : IAuditedEntity
    {
        public int ID { get; set; }

        [Display(Name = "Type")]
        public string Type { get; set; }

        [Display(Name = "Pris")]
        [Required(ErrorMessage = "Feltet må fylles ut")]
        [RegularExpression(@"^[1-9][0-9]{1,}$", ErrorMessage = "Prisen kan ikke være under 10 kr")]
        public int Value { get; set; }

        [Display(Name = "Laget av")]
        public string CreatedBy { get; set; }
        [Display(Name = "Tid laget")]
        public DateTime CreatedAt { get; set; }
        [Display(Name = "Sist endret av")]
        public string LastModifiedBy { get; set; }
        [Display(Name = "Sist endret")]
        public DateTime LastModifiedAt { get; set; }
    }
}