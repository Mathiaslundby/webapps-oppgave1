﻿
function hourToInt(hour) {
    return parseInt(hour.trim().split(":")[0]);
}

function diffDates(d1, d2) {
    let a = moment(d1, DATE_FORMAT);
    let b = moment(d2, DATE_FORMAT);
    return a.diff(b, "days");
}
