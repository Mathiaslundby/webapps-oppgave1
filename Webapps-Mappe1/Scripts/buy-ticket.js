﻿function choosePaymentMethod() {
    $('.modal-title').html(`Betaling`);
    $('.modal-body').html(`
        <label>Betalingsmetode</label>
        <select class='form-control'>
            <option>Vipps</option>
            <option>Visa</option>
            <option>Mastercard</option>
        </select>
        <div class='btn btn-primary' onclick='$("form").submit();'>Betal billett</div>
    `);
}

function setRoundTripTicketDateAndTime(date, time) {
    $.each($("#RoundTripTime option"), function (i, option) {
        if (hourToInt(option.value) == hourToInt(time)) {
            option.value = time;
            option.innerHTML = time;
        }
    });
    $("#RoundTripDate").val(date);
    $("#RoundTripTime").val(time);

    choosePaymentMethod();
}

function setReturnTicket() {
    let request = {
        EndStation: `${$('#StartStation').val()}`,
        StartStation: `${$('#EndStation').val()}`,
        StartTime: `${$('#StartTime').val()}`
    }

    $('.modal-title').html('Retur avganger');
    $.getJSON('/ticket/getdeparturejson', request, function (data) {
        $("tbody").html("");

        let chosenTime = hourToInt($('#RoundTripTime').val());
        if (hourToInt(data[0].StartTime) > chosenTime) {
            chosenTime = hourToInt(data[0].StartTime);
        }

        data.forEach(departure => {
            let start = hourToInt(departure.StartTime);
            let dateStr = $('#RoundTripDate').val();

            console.log(chosenTime, start);

            if (start >= chosenTime && start < (chosenTime + 5)) {
                $('tbody').append(`
                    <tr onclick="setRoundTripTicketDateAndTime('${dateStr}', '${departure.StartTime}')">
                        <td>${$('#EndStation').val()}</td>
                        <td>${$('#StartStation').val()}</td>
                        <td>${dateStr}</td>
                        <td>${departure.StartTime}</td>
                    </tr>
                `);
            }
            start++;
        });
    });
}

function setTicketDateAndTime(date, time) {
    $.each($("#StartTime option"), function (i, option) {
        if (hourToInt(option.value) == hourToInt(time)) {
            option.value = time;
            option.innerHTML = time;
        }
    });

    $("#StartDate").val(date);
    $("#StartTime").val(time);

    if (time > $('#RoundTripTime').val() && date == $('#RoundTripDate').val()) {
        $.each($("#RoundTripTime option"), function (i, option) {
            if (hourToInt(option.value) == hourToInt(time)) {
                option.value = time;
                option.innerHTML = time;
            }
        });
        $("#RoundTripTime").val(time);
    }

    if ($("#RoundTrip")[0].checked) {
        setReturnTicket();
    } else {
        choosePaymentMethod();
    }
}

function setTicket(event) {
    let from = $("#StartStation").val();
    let to = $('#EndStation').val();
    let time = $('#StartTime').val();

    if (!from || !to || !time) return;

    let request = {
        StartStation: `${from}`,
        EndStation: `${to}`,
        StartTime: `${time}`
    }

    $.getJSON('/ticket/getdeparturejson', request, function (data) {
        $('.modal-body').html(`
            <table class="table">
                <thead>
                    <tr>
                        <th>Fra</th>
                        <th>Til</th>
                        <th>Dato</th>
                        <th>Tid</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
`);
        $("tbody").html("");
        console.log(data);
        var chosenTime = hourToInt($('#StartTime').val());
        if (hourToInt(data[0].StartTime) > chosenTime) {
            chosenTime = hourToInt(data[0].StartTime);
        }
        data.forEach(departure => {
            let dateStr = $('#StartDate').val();
            let start = hourToInt(departure.StartTime);
            if (start >= chosenTime && start < (chosenTime + 5)) {
                $('tbody').append(`
                    <tr onclick="setTicketDateAndTime('${dateStr}', '${departure.StartTime}')">
                        <td>${$('#StartStation').val()}</td>
                        <td>${$('#EndStation').val()}</td>
                        <td>${dateStr}</td>
                        <td>${departure.StartTime}</td>
                    </tr>
                `);
            }
            start++;
        });
    });
}

$(document).ready(() => {
    $("#StartDate").val(moment().format(DATE_FORMAT));
    $("#RoundTripDate").val(moment().format(DATE_FORMAT));


    $('#RoundTrip').on('change', (event) => {
        if (event.target.checked) $('#RoundTripFields').removeClass('hidden');
        else $('#RoundTripFields').addClass('hidden');
    })

    $('.plus').click(function (event) {
        let input = $(this).prev();
        let value = parseInt(input.val()) + 1 || 1;
        input.val(value);
        $('#NumTicketsError').addClass('hidden');
    })

    $('.minus').click(function (event) {
        let input = $(this).next();
        let value = parseInt(input.val()) - 1 || 0;
        input.val(Math.max(0, value));
        $('#NumTicketsError').addClass('hidden');
    })

    $('input.numTickets').on('keydown', function (event) {
        if (event.key == 'ArrowDown') {
            if (this.value == 0) {
                event.preventDefault();
                return;
            }
        } else if (event.key === "ArrowUp") {
            if (this.value >= 9) {
                event.preventDefault();
                return;
            }
        } else if (event.key === "Tab") {
            return;
        } else {
            event.preventDefault();
        }

    });

    $(".datepicker").attr("autocomplete", "off");
    $(".datepicker").on("keydown", function (event) {
        if (event.key !== "Tab") {
            event.preventDefault();
        }
    });

    $(".datepicker").datepicker({
        language: 'no',
        startDate: moment().format(DATE_FORMAT)
    });

    $("#StartDate").on("changeDate", () => {
        let date = $("#StartDate").val();

        $("#RoundTripDate").datepicker("setStartDate", date);
        if (diffDates(date, $("#RoundTripDate").val() || date) > 0) {
            $("#RoundTripDate").val(date);
        }
    });

    $('#btn-departures').on('click', function () {
        if (!ticketIsValid()) {
            return;
        }
        $('.modal-title').html('Avganger');
        setTicket();
        $('.modal').modal();
    })
})
