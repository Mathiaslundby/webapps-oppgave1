﻿const DATE_FORMAT = "DD.MM.YYYY";

$(document).ready(() => {

    // Navbar activation
    let url = window.location.href.split("/");
    if (window.location.href.endsWith("ShowTickets")) {
        $('.navbar-nav li:not(#showTickets)').removeClass('active');
        $("#showTickets").addClass("active");
    } else if (url[url.length - 2] === "Details") {
        $('.navbar-nav li:not(#showTickets)').removeClass('active');
        $("#showTickets").addClass("active");
    } else if (window.location.href.includes("Ticket")) {
        $('.navbar-nav li:not(#showTickets)').removeClass('active');
        $("#buyTicket").addClass("active");
    } else if (window.location.href.includes("Admin")) {
        $('.navbar-nav li:not(#SignIn)').removeClass('active');
        $("#SignIn").addClass("active");
    } else {
        $('.navbar-nav li:not(#buyTicket)').removeClass('active');
        $("#SignIn").addClass("active");
    }

    $('#btnConfirm').on('click', function (event) {
        let hours = $('#hours').val();
        let minutes = $('#minutes').val();
        $('#StartTime').val(`${hours}:${minutes}`);

        $('form').submit();
    });

    $('#hours').on('keyup', function (event) {
        if (this.value.length == 2 && event.key.match("[0-9]")) {
            $('#minutes').focus();
        }
    });

    $('#minutes').on('keydown', function (event) {
        //console.log(event.key);
        if (this.value.length == 0 && event.key == "Backspace") {
            $('#hours').focus();
        }
        if (event.key == "e") {
            event.preventDefault();
        }
        else if (this.value.length == 2 && event.key.match("[0-9]")) {
            event.preventDefault();
        }
    })
})
