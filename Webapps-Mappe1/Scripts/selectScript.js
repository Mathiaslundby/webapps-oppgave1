﻿$(document).ready(() => {

    let selects = document.getElementsByClassName('select');

    for (let i = 0; i < selects.length; i++) {
        let options = selects[i].options;

        for (let j = 0; j < options.length; j++) {
            let str = options[j].value || options[j].innerHTML;
            str = str.split('_');
            str = str.join(' ');
            options[j].innerHTML = str;
        }
    }
})