﻿function ticketIsValid() {
    let valid = true;

    let fromError = $("#StartStation").parent().find('.error');
    let toError = $('#EndStation').parent().find('.error');
    let startDateError = $('#StartDate').parent().find('.error');
    let startTimeError = $('#StartTime').parent().find('.error');
    let roundTripDateError = $('#RoundTripDate').parent().find('.error');
    let roundTripTimeError = $('#RoundTripTime').parent().find('.error');
    let numTicketsError = $('#NumTicketsError');
    let emailError = $('#EmailError');

    if (!$('#Email').val().match(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/)) {
        emailError.removeClass('hidden').html("Ugyldig epostaddresse.");
        valid = false;
    }

    if ($('#StartStation').val() === '') {
        fromError.removeClass('hidden').html("Du må velge stasjon.");
        valid = false;
    }

    if ($('#EndStation').val() === '') {
        toError.removeClass('hidden').html("Du må velge destinasjon.");
        valid = false;
    } else if ($('#EndStation').val() === $('#StartStation').val()) {
        toError.removeClass('hidden').html("Stasjonene kan ikke være like.");
        valid = false;
    }

    if ($('#StartDate').val() === '') {
        startDateError.removeClass('hidden').html('Avreise dato må være satt.');
        valid = false;
    }

    if ($('#StartTime').val() === '') {
        startTimeError.removeClass('hidden').html('Avreise tidspunkt må være satt.');
        valid = false;
    }

    if ($('#RoundTrip')[0].checked) {

        if ($('#RoundTripDate').val() === '') {
            roundTripDateError.removeClass('hidden').html('Avreise dato må være satt.');
            valid = false;
        }

        if ($('#RoundTripTime').val() === '') {
            roundTripTimeError.removeClass('hidden').html('Avreise tidspunkt må være satt.');
            valid = false;
        } else {
            if ($('#StartDate').val() === $('#RoundTripDate').val()) {
                if (hourToInt($('#StartTime').val()) >= hourToInt($('#RoundTripTime').val())) {
                    roundTripTimeError.removeClass('hidden').html('Kan ikke reise tilbake i tid.');
                    valid = false;
                }
            }
        }
    }

    let adults = parseInt($("#NumAdults").val());
    let children = parseInt($("#NumChildren").val());
    let students = parseInt($("#NumStudents").val());
    let seniors = parseInt($("#NumSeniors").val());
    if (adults + children + students + seniors < 1) {
        numTicketsError.removeClass('hidden').html('Du må velge antall billetter.');
        valid = false;
    }
    return valid;
}

$(document).ready(() => {
    $('#StartStation').on('change', function (event) { $(this).parent().find('.error').addClass('hidden') });
    $('#EndStation').on('change', function (event) { $(this).parent().find('.error').addClass('hidden') });
    $('#StartDate').on('change', function (event) { $(this).parent().find('.error').addClass('hidden') });
    $('#StartTime').on('change', function (event) { $(this).parent().find('.error').addClass('hidden') });
    $('#RoundTripDate').on('change', function (event) { $(this).parent().find('.error').addClass('hidden') });
    $('#RoundTripTime').on('change', function (event) { $(this).parent().find('.error').addClass('hidden') });
    $('.num-tickets input').on('change', function (event) { $('#NumTicketsError').addClass('hidden') });
    $('#Email').on('focus', function (event) { $('#EmailError').addClass('hidden') });
});
