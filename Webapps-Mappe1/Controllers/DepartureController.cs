﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Webapps_Mappe1.Model;
using Webapps_Mappe1.BLL;
using System.IO;

namespace Webapps_Mappe1.Controllers
{
    public class DepartureController : Controller
    {
        DepartureBLL departureBLL;

        public DepartureController()
        {
            departureBLL = new DepartureBLL();
        }

        public DepartureController(DepartureBLL departureBLLStub)
        {
            departureBLL = departureBLLStub;

        }

        
        // GET: Departure
        public ActionResult Index()
        {
            return View(departureBLL.GetAllDepartures());
        }

        public ActionResult Reset()
        {
            departureBLL.Reset();
            return RedirectToAction("Station/Index");
        }

        public ActionResult EditDeparture(int id)
        {
            return View(departureBLL.GetDeparture(id));
        }

        [HttpPost]
        public ActionResult EditDeparture(int id, Departure departure)
        {
            if (ModelState.IsValid)
            {
                if (departureBLL.EditDeparture(id, departure))
                {
                    return RedirectToAction("Index");
                }
            }
            return View();
        }

        public void DeleteDeparture(int id)
        {
            departureBLL.DeleteDeparture(id);
        }

        public ActionResult CreateDeparture()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateDeparture(DepartureModel departure)
        {

            /*   if (Session["SignIn"] != null) //Har den her også selv om det ikke er en egen side, vet ikke om det er nødvendig
               {
                   bool SignIn = (bool)Session["SignIn"];
                   if (!SignIn)
                   {
                       return RedirectToAction("Index"); skal redirecte til admin index
                   }
               }*/
            if (ModelState.IsValid)
            {
                if (departureBLL.CreateDeparture(departure))
                    return RedirectToAction("Index");
            }
            return View();
        }
        protected override void OnException(ExceptionContext filterContext)
        {
            using (StreamWriter w = System.IO.File.AppendText(Server.MapPath(@"error.log")))
            {
                Log(filterContext.Exception.ToString(), w);
            }
            base.OnException(filterContext);
        }

        private void Log(string logMessage, TextWriter w)
        {
            w.WriteLine($"DepartureController");
            w.Write("\r\nLog Entry: ");
            w.WriteLine($"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()}\r\n");
            w.WriteLine($" {logMessage}\r\n");
            w.WriteLine("-----------------------------");
        }
    }
}