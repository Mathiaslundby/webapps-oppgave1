﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Webapps_Mappe1.Model;
using Webapps_Mappe1.BLL;
using System.IO;

namespace Webapps_Mappe1.Controllers
{
    public class PriceController : Controller
    {

        PriceBLL priceBLL;

        public PriceController()
        {
            priceBLL = new PriceBLL();
        }

        public PriceController(PriceBLL priceBLLStub)
        {
            priceBLL = priceBLLStub;

        }
        // GET: Price
        public ActionResult Index()
        {
            return View(priceBLL.GetAllPrices());
        }


        public ActionResult EditPrice(int id)
        {
            return View(priceBLL.GetPrice(id));
        }

        [HttpPost]
        public ActionResult EditPrice(int id, Price inPrice)
        {
            if (ModelState.IsValid)
            {
                if (priceBLL.EditPrice(id, inPrice))
                {
                    return RedirectToAction("Index");
                }
            }
            return View();
        }
        protected override void OnException(ExceptionContext filterContext)
        {
            using (StreamWriter w = System.IO.File.AppendText(Server.MapPath(@"error.log")))
            {
                Log(filterContext.Exception.ToString(), w);
            }
             base.OnException(filterContext);
        }

        private void Log(string logMessage, TextWriter w)
        {
            w.WriteLine($"PriceController");
            w.Write("\r\nLog Entry: ");
            w.WriteLine($"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()}\r\n");
            w.WriteLine($" {logMessage}\r\n");
            w.WriteLine("-----------------------------");
        }
    }
}