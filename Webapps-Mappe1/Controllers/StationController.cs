﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Webapps_Mappe1.Model;
using Webapps_Mappe1.BLL;
using System.IO;

namespace Webapps_Mappe1.Controllers
{
    public class StationController : Controller
    {
        StationBLL stationBLL;

        public StationController()
        {
            stationBLL = new StationBLL();
        }

        public StationController(StationBLL stationBLLStub)
        {
            stationBLL = stationBLLStub;
        }
            // GET: Station
        public ActionResult Index()
        {
            return View(stationBLL.GetAllStations());
        }

        public ActionResult CreateStation()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateStation(Station station)
        {
            if (ModelState.IsValid)
            {
                if (stationBLL.CreateStation(station))
                {
                    return RedirectToAction("Index");
                }
            }
            return View();
        }


        public void DeleteStation(int id)
        {
            stationBLL.DeleteStation(id);
        }

        public ActionResult EditStation(int id)
        {
            return View(stationBLL.GetStation(id));
        }


        [HttpPost]
        public ActionResult EditStation(int id, Station station)
        {
            if (ModelState.IsValid)
            {
                if (stationBLL.EditStation(id, station))
                {
                    return RedirectToAction("Index");
                }
            }
            return View();
        }
        protected override void OnException(ExceptionContext filterContext)
        {
            using (StreamWriter w = System.IO.File.AppendText(Server.MapPath(@"error.log")))
            {
                Log(filterContext.Exception.ToString(), w);
            }
             base.OnException(filterContext);
        }

        private void Log(string logMessage, TextWriter w)
        {
            w.WriteLine($"StationController");
            w.Write("\r\nLog Entry: ");
            w.WriteLine($"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()}\r\n");
            w.WriteLine($" {logMessage}\r\n");
            w.WriteLine("-----------------------------");
        }
    }
}