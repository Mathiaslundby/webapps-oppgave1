﻿using System;
using System.Web.Mvc;

using Webapps_Mappe1.Model;
using Webapps_Mappe1.BLL;
using System.IO;

namespace Webapps_Mappe1.Controllers
{
    public class AdminController : Controller
    {
        StationBLL stationBLL;
        DepartureBLL departureBLL;
        PriceBLL priceBLL;
        AdminBLL userBLL;

        public AdminController()
        {
            stationBLL = new StationBLL();
            departureBLL = new DepartureBLL();
            priceBLL = new PriceBLL();
            userBLL = new AdminBLL();
        }

        public AdminController(PriceBLL priceBLLStub)
        {
            priceBLL = priceBLLStub;
        }

        public ActionResult Index()
        {
            if (userBLL.CheckAdminExists())
            {
                userBLL.createAdmin();
                Session["SignIn"] = false;
            }


            if (Session["SignIn"] == null || !(bool)Session["SignIn"])
            {
                Session["SignIn"] = false;
                ViewBag.Signedin = false;
            }
            else
            {
                ViewBag.Signedin = (bool)Session["SignIn"];
                return RedirectToAction("AdminPage");
            }

            return View();
        }

        [HttpPost]
        public ActionResult Index(Admin inUser)
        {
            DBUser foundUser = userBLL.UserInDB(inUser);
            if (foundUser != null)
            {
                Session["SignIn"] = true;
                ViewBag.Signedin = inUser.Name;
                Session["username"] = inUser.Name;
                return RedirectToAction("AdminPage");
            }
            else
            {
                Session["SignIn"] = false;
                ViewBag.Signedin = "Else";
                Session["username"] = "Automatisk";
                return View();
            }
        }


        public ActionResult AdminPage()
        {
            if (Session["SignIn"] != null)
            {
                bool SignIn = (bool)Session["SignIn"];
                if (SignIn)
                {
                    return View();
                }
            }
            return RedirectToAction("Index");
        }


        protected override void OnException(ExceptionContext filterContext)
        {
            using (StreamWriter w = System.IO.File.AppendText(Server.MapPath(@"error.log")))
            {
                Log(filterContext.Exception.ToString(), w);
            }
            base.OnException(filterContext);
        }

        private void Log(string logMessage, TextWriter w)
        {
            w.WriteLine($"AdminController");
            w.Write("\r\nLog Entry: ");
            w.WriteLine($"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()}\r\n");
            w.WriteLine($" {logMessage}\r\n");
            w.WriteLine("-----------------------------");
        }
    }
}
