﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Webapps_Mappe1.BLL;
using Webapps_Mappe1.Model;

namespace Webapps_Mappe1.Controllers
{
    public class TicketController : Controller
    {
        private readonly StationBLL stationBLL;
        private readonly DepartureBLL departureBLL;

        TicketBLL ticketBLL;

        public TicketController()
        {
            ticketBLL = new TicketBLL();
            stationBLL = new StationBLL();
            departureBLL = new DepartureBLL();
    }

        public TicketController(TicketBLL ticketBLLStub)
        {
            ticketBLL = ticketBLLStub;

        }
        public ActionResult Index()
        {

           /* StationBLL stationBLL = new StationBLL();

            if(stationBLL.CheckStationsNotExists())
            {
                CreateStations();
                CreateDepartures();
                CreatePrices();
            }*/
            return View();
        }

        [HttpPost]
        public ActionResult Index(Ticket postTicket)
        {
            if (ModelState.IsValid)
            {
                if (ticketBLL.createNewTicket(postTicket))
                {
                    return RedirectToAction("ShowTickets");
                }
            }
            return View();
        }
        
        public ActionResult ShowTickets()
        {
            List<DBTicket> tickets = ticketBLL.GetTickets();
            return View(tickets);
        }

        public ActionResult Details(int id)
        {
            DBTicket ticket = ticketBLL.findTicket(id);
            return View(ticket);
        }
        
        public JsonResult GetStationsJson()
        {
            JsonResult output = Json(stationBLL.GetAllStations(), JsonRequestBehavior.AllowGet);
            return output;
        }

        public JsonResult GetDepartureJson(DepartureModel departure)
        {
            JsonResult output = Json(departureBLL.GetDepartures(departure), JsonRequestBehavior.AllowGet);
            return output;
        }

       private void CreateDepartures()
        {
            DepartureBLL departureBLL = new DepartureBLL();
            departureBLL.CreateDepartures();
        }


        
       /* private void CreateStations()

        {
            StationBLL stationBLL = new StationBLL();
            stationBLL.CreateStations();
        }
        */
        private void CreatePrices()
        {
            PriceBLL priceBLL = new PriceBLL();
            priceBLL.CreatePrices();
        }
        
        public ActionResult Reset()
        {
            AdminBLL userBLL = new AdminBLL();
            userBLL.Reset();
            return RedirectToAction("ShowTickets");
        }
        
        protected override void OnException(ExceptionContext filterContext)
        {
            using (StreamWriter w = System.IO.File.AppendText(Server.MapPath(@"error.log")))
            {
                Log(filterContext.Exception.ToString(), w);
            }
             base.OnException(filterContext);
        }

        private void Log(string logMessage, TextWriter w)
        {
            w.WriteLine($"TicketController");
            w.Write("\r\nLog Entry: ");
            w.WriteLine($"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()}\r\n");
            w.WriteLine($" {logMessage}\r\n");
            w.WriteLine("-----------------------------");
        }
    }
}

